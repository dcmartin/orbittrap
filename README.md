# README #

### What is this repository for? ###

An AJAX implementation of a the orbit trap fractal visualizer, with added features to save, load, and vote on individual discoveries.  

### How do I get set up? ###

Navigating a modern browser to fractals.html on any system that has all of the source files in this structure is all that is necessary to begin exploring fractals.  To save and load them a mysql database is required, and the php files need to be pointed to that database.

### Who do I talk to? ###

dcmartin@monogon.net