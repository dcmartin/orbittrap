

<?php
// mysql database on monogon.net:
// db:  horrabin_orbittrap
// user:  horrabin_ot
// pass:  horrabin_pw
// table:  fractals
$servername = "localhost";
$username = "horrabin_ot";
$password = "horrabin_pw";
$dbname = "horrabin_orbittrap";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// $sql = "SELECT * FROM fractals";
$sql = "SELECT * FROM `fractals` ORDER BY `love` DESC LIMIT 2";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
  echo '{';
    while($row = $result->fetch_assoc()) {
      echo '"' . $row['id'] . '":{ "name":"' . $row['name'] . '", "a":' . $row['a'] . ', "b":' . $row['b'] . ', "c":' . $row['c']
       . ', "d":' . $row['d'] . ', "e":' . $row['e'] . ', "f":' . $row['f'] . ', "g":' . $row['g'] . ', "h":' 
       . $row['h'] . ', "j":' . $row['j'] . ', "r":' . $row['r'] . ', "w":' . $row['w'] . ', "n":' . $row['n'] 
       . ', "shiftx":' . $row['shiftx'] . ', "shifty":' . $row['shifty'] . ', "shifth":' . $row['shifth'] 
       . ', "zoom":' . $row['zoom'] . ', "love":' . $row['love'] . '},' ;
        // echo 'id: ' . $row['id']. ' - Name: ' . $row['firstname']. ' ' . $row['lastname']. '<br>';
    }
  echo '}';
} else {
    echo '0 results';
}
$conn->close();
?>